import logging, sys, re, requests, time, math

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, ParseMode
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

# Get the token from script parameters
bot_token = sys.argv[1]

STATE_CHOICE, STATE_RESTAU_CHOICE, STATE_OUT_CHOICE, STATE_OUT_RECOMMENDATION, STATE_RESTAU_RESULTS, STATE_RESTAU_DETAILS, STATE_TRANSPORT = range(7)

bars = [
    "<b>L'éléphant dans la cannette</b> \nAvenue du mail 18 \n1205 Genève \ntu trouveras plus d'infos ici : https://elephantdanslacanette.ch/",
    "<b>Le Kraken</b> \nRue de l'école de médecine 5 \n1205 Genève \ntu trouveras plus d'infos ici : http://www.lekrakenbar.ch/",
    "<b>L'établi</b> \nRue de l'école de médecine 3 \n1205 Genève \ntu trouveras plus d'infos ici : https://fr.tripadvisor.ch/Restaurant_Review-g188057-d7178213-Reviews-Bar_L_etabli-Geneva.html",
]
clubs = [
    "<b>L'usine</b> \nPlace des Volontaires 4 \n1204 Genève \ntu trouveras plus d'infos ici : https://www.usine.ch/",
    "<b>La parfumerie</b> \nChemin de la Gravière 7 \n1227 Genève \ntu trouveras plus d'infos ici : https://www.laparfumerie.ch/",
    "<b>L'audio</b> \nRue Boissonnas 20 \n1227 Genève \ntu trouveras plus d'infos ici : https://audio-club.ch/",
]
musees = [
    "<b>Musée d'ethnographie</b> \nBoulevard Carl-Vogt 65 \n1205 Genève \ntu trouveras plus d'infos ici : https://www.ville-ge.ch/meg/",
    "<b>Musée d'histoire naturelle</b> \nRoute de Malagnou 1 \n1208 Genève \ntu trouveras plus d'infos ici : http://institutions.ville-geneve.ch/fr/mhn/",
    "<b>Musée ariana</b> \nAvenue de la Paix 10 \n1202 Genève \ntu trouveras plus d'infos ici : http://institutions.ville-geneve.ch/fr/ariana/",
]

transport_base_url = "http://transport.opendata.ch/v1/"

def start(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [['Restaurant', 'Sorties']]

    update.message.reply_text(
        'Salut ! Je suis le Geneva bot ! Je suis là pour t\'aider à faire les bons choix d\'activités \n'
        'En m\'envoyant la commande /transport tu peux obtenir les prochains départs de transports publics autour de toi\n'
        'Tu peux à tout moment terminer la conversation en m\'envoyant /cancel\n\n'
        'Quelle activité souhaite-tu effectuer ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return STATE_CHOICE

def restaurant_choice(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    reply_keyboard = [
        ['Asiatique', 'Italien', 'Burger', 'Thai', 'Brasserie'],
        ['Retour']
    ]

    logger.info("%s Choosed restaurant", user.first_name)

    update.message.reply_text(
        'Ok ! Quel type de restaurant te tente ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return STATE_RESTAU_CHOICE

def restaurant_results(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    reply_keyboard = [
        ['Inglewood', 'Dei Trulli', 'Les Tilleuls', 'La petite Vendée', 'Café de la place'],
        ['Retour']
    ]

    logger.info("%s Choosed a restaurant type", user.first_name)

    update.message.reply_text(
        f"Voilà les restaurants de type {update.message.text} que j'ai trouvé pour toi",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )
    return STATE_RESTAU_RESULTS

def restaurant_details(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    reply_keyboard = [['Autres restaurants']]

    logger.info("%s Choosed a restaurant", user.first_name)
    update.message.reply_text(
        f"Voilà les détails pour \"{update.message.text}\" \n\n"
        "Et il est noté 4.5/5 sur TripAdvisor, si tu veux mon avis c'est un bon choix",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )
    update.message.reply_location(46.194129, 6.139977)

    return STATE_RESTAU_DETAILS

def out_choice(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    reply_keyboard = [
        ['Restaurant', 'Musées', 'Bars', 'Clubs'],
        ['Retour']
    ]

    logger.info("%s Choosed go out", user.first_name)

    update.message.reply_text(
        'Ok ! Quel genre de sortie ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    return STATE_OUT_CHOICE

def out_recommendations(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    text = update.message.text
    reply_keyboard = [
        ['Retour']
    ]
    recommendations = []

    logger.info("%s is getting \"go out\" recommendations", user.first_name)

    update.message.reply_text(
        f'Très bien, alors voilà quelques {text} qui pourraient te plaire...',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )

    if re.compile('^(Musée)').match(text):
        recommendations = musees
    elif re.compile('^(Bar)').match(text):
        recommendations = bars
    elif re.compile('^(Club)').match(text):
        recommendations = clubs

    for recommendation in recommendations:
        update.message.reply_text(recommendation, parse_mode=ParseMode.HTML)

    return STATE_OUT_RECOMMENDATION

def start_transport(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user

    logger.info("User %s choosed transport", user.first_name)

    update.message.reply_text(
        "Indique-moi le nom d'un arrêt ou envoie-moi ta localisation et je te donnerai les prochains départs de transports publics",
        reply_markup=ReplyKeyboardRemove()
    )

    return STATE_TRANSPORT

def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user

    logger.info("User %s canceled the conversation.", user.first_name)

    update.message.reply_text(
        'À bientôt !', reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END


def call_transport_api(endpoint, parameters):
    query = transport_base_url + endpoint + "?"
    for parameter in parameters:
        query += parameter[0] + "=" + str(parameter[1]) + "&"
    print("executing : " + query)
    result = requests.get(query)
    return result.json()

def show_stops(stops, update):
    for stop in stops['stations']:
        if stop['id'] is not None:
            update.message.reply_text("/stop" + stop['id'] + " - " + stop['name'])

def compute_time(timestamp):
    seconds = timestamp - time.time()
    minutes = seconds / 60
    minutes = math.floor(minutes)

    if minutes < 0:
        return "Déjà passé"

    if(minutes < 3):
        return "Vite ! {} min".format(minutes)

    return "{} min".format(minutes)

def search_by_location(update: Update, context: CallbackContext) -> None:
    location = update.message.location
    parameters = [["x", location.longitude], ["y", location.latitude]]
    stops = call_transport_api("locations", parameters)
    show_stops(stops, update)

def search_by_name(update: Update, context: CallbackContext) -> None:
    parameters = [["query", update.message.text]]
    stops = call_transport_api("locations", parameters)
    show_stops(stops, update)


def show_timetable(update: Update, context: CallbackContext) -> None:
    stop_id = update.message.text[5:]
    parameters = [["id", stop_id], ["limit", "10"]]
    next_departures = call_transport_api("stationboard", parameters)

    response = "Prochains départs:\n\n"

    if next_departures["stationboard"] is None:
        response = "Je n'ai malheureusement trouvé aucun arrêt proche de la localisation que tu m'as envoyé... \n" \
                   "Je reste ouvert à d'autres requêtes ! "

    for departure in next_departures["stationboard"]:
        stop = departure['stop']
        response = response + departure['number'] +\
                   " -> " + departure['to'] +\
                   " <b>" + compute_time(stop['departureTimestamp']) + "</b>\n"

    response = response + "\nPour actualiser: /stop{}".format(stop_id)
    update.message.reply_text(response, parse_mode=ParseMode.HTML)


def main() -> None:
    # Create the Updater and pass it your bot's token.
    updater = Updater(bot_token)
    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            STATE_CHOICE: [
                MessageHandler(Filters.regex('^(Restaurant)$'), restaurant_choice),
                MessageHandler(Filters.regex('^(Sorties)$'), out_choice),
            ],
            STATE_RESTAU_CHOICE: [
                MessageHandler(Filters.regex('^(Asiat|Ital|Burger|Surprend)'), restaurant_results),
                MessageHandler(Filters.regex('^(Retour|Back)$'), start),
            ],
            STATE_RESTAU_RESULTS: [
                MessageHandler(Filters.regex('^(Retour|Back)$'), restaurant_choice),
                MessageHandler(Filters.regex('(.*)'), restaurant_details),
            ],
            STATE_RESTAU_DETAILS: [
                MessageHandler(Filters.regex('^(Autres)'), restaurant_results),
            ],
            STATE_OUT_CHOICE: [
                MessageHandler(Filters.regex('^(Restaurant)$'), restaurant_choice),
                MessageHandler(Filters.regex('^(Retour|Back)$'), start),
                MessageHandler(Filters.regex('^(Restaurant|Musée|Bar|Club)'), out_recommendations),
            ],
            STATE_OUT_RECOMMENDATION: [
                MessageHandler(Filters.regex('^(Retour|Back)$'), out_choice),
            ]
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    transport_handler = ConversationHandler(
        entry_points=[CommandHandler('transport', start_transport)],
        states={
            STATE_TRANSPORT: [
                MessageHandler(Filters.location, search_by_location),
                MessageHandler(Filters.command, show_timetable),
                MessageHandler(Filters.text, search_by_name)
            ],
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(transport_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
